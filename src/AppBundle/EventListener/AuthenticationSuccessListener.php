<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.11.2016.
 * Time: 0:46
 */

namespace AppBundle\EventListener;
use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthenticationSuccessListener
{
    protected $uploadHelper;

    public function __construct($uploadHelper)
    {
        $this->uploadHelper = $uploadHelper;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $path = $this->uploadHelper->asset($user, 'profilePictureFile');

        $data['data'] = array(
            'role'              =>  $user->getRoles()[0],
            'username'          =>  $user->getUsername(),
            'email'             =>  $user->getEmail(),
            'profilePicture'    =>  $path,
        );

        $event->setData($data);
    }
}