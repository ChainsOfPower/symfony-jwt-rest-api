<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 8.11.2016.
 * Time: 4:11
 */

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\RegistrationType;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\RestBundle\View\View;

class SecurityController extends FOSRestController
{
    /**
     * @Route("/api/register")
     * @Method({"POST"})
     */
    public function registerAction(Request $request)
    {
        $formFactory = $this->get('fos_user.registration.form.factory');

        $userManager = $this->get('fos_user.user_manager');

        $dispatcher = $this->get('event_dispatcher');

        $cityRepository = $this->get('doctrine')->getRepository('AppBundle:City');

        $geo = $this->get('bazinga_geocoder.geocoder');
        $geo->limit(1);

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setAccountBalance(0);

        $event = new \FOS\UserBundle\Event\GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(\FOS\UserBundle\FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if(null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if($form->isValid()){
            $event = new \FOS\UserBundle\Event\FormEvent($form, $request);
            $dispatcher->dispatch(\FOS\UserBundle\FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $role = $form->get('role')->getData();
            if($role === 'customer')
            {
                $user->setRoles(array('ROLE_CUSTOMER'));
            }else if($role === 'seller')
            {
                $user->setRoles(array('ROLE_SELLER'));
            }else if($role === 'deliverer')
            {
                $user->setRoles(array('ROLE_DELIVERER'));
            }
            $location = $geo->reverse($user->getLat(), $user->getLon());
            $cityRepository->updateCity($location->get(0)->getAdminLevels()->get(1)->getName(),$location->get(0)->getLocality(), $location->get(0)->getPostalCode());
            $cityRepository->updateUserCity($user, $location->get(0)->getAdminLevels()->get(1)->getName(), $location->get(0)->getLocality());

            $userManager->updateUser($user);

            if(null === $response = $event->getResponse())
            {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new \Symfony\Component\HttpFoundation\RedirectResponse($url);
            }

            $dispatcher->dispatch(\FOS\UserBundle\FOSUserEvents::REGISTRATION_COMPLETED, new \FOS\UserBundle\Event\FilterUserResponseEvent($user, $request, $response));

            $view = $this->view(array('message' => 'registration success'), Response::HTTP_CREATED);

            return $this->handleView($view);
        }

        $view = $this->view($form, Response::HTTP_BAD_REQUEST);
        return $this->handleView($view);
    }

    /**
     * @Route("/api/reset_password")
     * @Method({"POST"})
     */
    public function changePasswordAction(Request $request)
    {
        $user = $this->getUser();
        if(!is_object($user) || !$user instanceof UserInterface)
        {
            throw new AccessDeniedException('This user does not have access to this section');
        }

        $dispatcher  = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);

        if(null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);


        $form->handleRequest($request);

        if($form->isValid()) {
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

            $userManager->updateUser($user);

            if(null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            $view = $this->view(array('message' => 'password change success'), Response::HTTP_OK);

            return $this->handleView($view);
        }

        $view = $this->view($form, Response::HTTP_BAD_REQUEST);
        return $this->handleView($view);

    }

    /**
     * @Route("/api/logout")
     * @Method({"POST"})
     */
    public function logout(Request $request)
    {
        $token = $request->request->get('token');
        $em = $this->getDoctrine()->getManager();
        $tokenManager = $em->getRepository('GesdinetJWTRefreshTokenBundle:RefreshToken');

        $tokenEntity = $tokenManager->findOneBy(array('refreshToken' => $token));


        if($tokenEntity == null)
        {
            $view = $this->view(array('message' => 'invalid token'), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        $em->remove($tokenEntity);
        $em->flush();

        $view = $this->view(array('message' => 'logout success'), Response::HTTP_OK);

        return $this->handleView($view);
    }

}