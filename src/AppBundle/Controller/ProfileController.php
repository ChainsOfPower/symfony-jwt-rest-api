<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.11.2016.
 * Time: 17:04
 */

namespace AppBundle\Controller;


use AppBundle\Form\ProfileType;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\User;
use AppBundle\Form\RegistrationType;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\HttpFoundation\Response;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\RestBundle\View\View;

class ProfileController extends FOSRestController
{
    /**
     * @Route("/api/profile")
     * @Method({"POST", "GET"})
     */
    public function profileEditAction(Request $request)
    {
        $user = $this->getUser();
        if(!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section');
        }

        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if(null != $event->getResponse()) {
            return $event->getResponse();
        }

        $formFactory = $this->get('fos_user.profile.form.factory');
        $form = $formFactory->createForm();
        $form->setData($user);

        if($request->isMethod('POST')) {
            $form->submit(array('lat' => $request->request->get('lat'),
                'lon' => $request->request->get('lon'),
                'profilePictureFile' => array('file' => $request->files->get('profilePictureFile'))));

            if($form->isSubmitted() && $form->isValid()) {
                $userManager = $this->get('fos_user.user_manager');

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_SUCCESS, $event);

                $userManager->updateUser($user);

                if(null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_profile_show');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));


                $view = $this->view(array('token' => "successful edit"), Response::HTTP_OK);
                return $this->handleView($view);
        }
            $view = $this->view($form, Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $path = $helper->asset($user, 'profilePictureFile');

        $view = $this->view(array('username' => $user->getUsername(), 'email' => $user->getEmail(), 'role' => $user->getRoles()[0], 'profilePicture' => $path, 'lon' => $user->getLon(), 'lat' => $user->getLat()), Response::HTTP_OK);
        return $this->handleView($view);
    }


}