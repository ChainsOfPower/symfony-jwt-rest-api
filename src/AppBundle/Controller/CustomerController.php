<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.11.2016.
 * Time: 17:45
 */

namespace AppBundle\Controller;


use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends FOSRestController
{
    /**
     * @Route("/api/customer/test")
     * @Method({"POST"})
     * @Security("has_role('ROLE_CUSTOMER')")
     */
    public function customerAction(Request $request)
    {
        $this->view(array('message' => 'CUSTOMERS ONLY'));

    }
}