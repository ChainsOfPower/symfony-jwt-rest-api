<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.10.2016.
 * Time: 1:22
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @Vich\Uploadable()
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="lat", type="float")
     * @Assert\NotBlank(message="You have to specify latitude", groups={"Registration", "Profile"})
     */
    protected $lat;

    /**
     * @ORM\Column(name="lon", type="float")
     * @Assert\NotBlank(message="You have to specify longitude", groups={"Registration", "Profile"})
     */
    protected $lon;

    /**
     * @Vich\UploadableField(mapping="profile_image", fileNameProperty="profilePicture")
     */
    protected $profilePictureFile;

    /**
     * @ORM\Column(type="string", name="profile_picture", length=255, nullable=true)
     */
    protected $profilePicture;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City", inversedBy="users")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @var float
     *
     * @ORM\Column(name="accountBalance", type="float")
     * @Assert\NotBlank(message="Account balance can't be blank")
     * @Assert\Range(
     *     min=0,
     *     max=10000000,
     *     minMessage="Account balance can't be negative value",
     *     maxMessage="Account balance can't be greater than 10000000")
     */
    private $accountBalance;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=30, nullable=true)
     * @Assert\Regex(
     *     pattern="/^\d+&/",
     *     message="Phone number has to be a number"
     * )
     */
    private $phoneNumber;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\FireBase", mappedBy="user")
     */
    private $tokens;

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

    public function __construct()
    {
        parent::__construct();
        $this->tokens = new ArrayCollection();
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($latitude)
    {
        $this->lat = $latitude;

        return $this;
    }

    public function getLon()
    {
        return $this->lon;
    }

    public function setLon($longitude)
    {
        $this->lon = $longitude;

        return $this;
    }

    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    public function setProfilePicture($profilePicture)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }
    public function getProfilePictureFile()
    {
        return $this->profilePictureFile;
    }

    public function setProfilePictureFile(File $image = null)
    {
        $this->profilePictureFile = $image;

        if($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return User
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set accountBalance
     *
     * @param float $accountBalance
     *
     * @return User
     */
    public function setAccountBalance($accountBalance)
    {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * Get accountBalance
     *
     * @return float
     */
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Add token
     *
     * @param \AppBundle\Entity\FireBase $token
     *
     * @return User
     */
    public function addToken(\AppBundle\Entity\FireBase $token)
    {
        $this->tokens[] = $token;

        return $this;
    }

    /**
     * Remove token
     *
     * @param \AppBundle\Entity\FireBase $token
     */
    public function removeToken(\AppBundle\Entity\FireBase $token)
    {
        $this->tokens->removeElement($token);
    }

    /**
     * Get tokens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTokens()
    {
        return $this->tokens;
    }
}
