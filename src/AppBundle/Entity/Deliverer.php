<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Deliverer
 *
 * @ORM\Table(name="deliverer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DelivererRepository")
 */
class Deliverer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="companyName", type="string", length=60)
     * @Assert\NotBlank(message="Company name can't be blank")
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="creditCardNumber", type="string", length=30)
     * @Assert\NotBlank(message="Credit card number can't be blank")
     * @Assert\Regex(
     *     pattern="/^\d+&/",
     *     message="Credit card number has to be a number"
     * )
     */
    private $creditCardNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="vehicleType", type="string", length=20)
     * @Assert\NotBlank(message="You have to specify vehicle type")
     * @Assert\Choice({"plain", "cooled"}, message="Type of vehicle has to be plain or cooled")
     */
    private $vehicleType;

    /**
     * @var int
     *
     * @ORM\Column(name="maxTravelDistance", type="integer")
     * @Assert\NotBlank(message="Maximum travel distance can't be blank")
     * @Assert\Range(
     *     min=10,
     *     max=1000,
     *     minMessage="Maximum travel distance can't be less than 10 kilometers",
     *     maxMessage="Maximum travel distance can't be greater than 1000 kilometers"
     * )
     */
    private $maxTravelDistance;

    /**
     * @var float
     *
     * @ORM\Column(name="pricePerKilometer", type="float")
     * @Assert\NotBlank()
     * @Assert\Range(
     *     min = 0,
     *     max = 30,
     *     minMessage="Price per kilometer can't be less than 0kn",
     *     maxMessage="Price per kilometer can't be greater than 30kn"
     * )
     */
    private $pricePerKilometer;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Deliverer
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set creditCardNumber
     *
     * @param string $creditCardNumber
     *
     * @return Deliverer
     */
    public function setCreditCardNumber($creditCardNumber)
    {
        $this->creditCardNumber = $creditCardNumber;

        return $this;
    }

    /**
     * Get creditCardNumber
     *
     * @return string
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    /**
     * Set accountBalance
     *
     * @param float $accountBalance
     *
     * @return Deliverer
     */
    public function setAccountBalance($accountBalance)
    {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * Get accountBalance
     *
     * @return float
     */
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }

    /**
     * Set vehicleType
     *
     * @param string $vehicleType
     *
     * @return Deliverer
     */
    public function setVehicleType($vehicleType)
    {
        $this->vehicleType = $vehicleType;

        return $this;
    }

    /**
     * Get vehicleType
     *
     * @return string
     */
    public function getVehicleType()
    {
        return $this->vehicleType;
    }

    /**
     * Set maxTravelDistance
     *
     * @param integer $maxTravelDistance
     *
     * @return Deliverer
     */
    public function setMaxTravelDistance($maxTravelDistance)
    {
        $this->maxTravelDistance = $maxTravelDistance;

        return $this;
    }

    /**
     * Get maxTravelDistance
     *
     * @return int
     */
    public function getMaxTravelDistance()
    {
        return $this->maxTravelDistance;
    }

    /**
     * Set pricePerKilometer
     *
     * @param float $pricePerKilometer
     *
     * @return Deliverer
     */
    public function setPricePerKilometer($pricePerKilometer)
    {
        $this->pricePerKilometer = $pricePerKilometer;

        return $this;
    }

    /**
     * Get pricePerKilometer
     *
     * @return float
     */
    public function getPricePerKilometer()
    {
        return $this->pricePerKilometer;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Deliverer
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Deliverer
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
