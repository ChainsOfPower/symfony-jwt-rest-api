<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Seller
 *
 * @ORM\Table(name="seller")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SellerRepository")
 */
class Seller
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="companyName", type="string", length=60 )
     * @Assert\NotBlank(message="Company name can't be blank")
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="creditCardNumber", type="string", length=30)
     * @Assert\NotBlank(message="Credit card number can't be blank")
     * @Assert\Regex(
     *     pattern="/^\d+&/",
     *     message="Credit card number has to be a number"
     * )
     */
    private $creditCardNumber;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="sellers")
     * @ORM\JoinTable(name="sellers_productCategories")
     */
    private $productCategories;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Offer", mappedBy="seller")
     */
    private $offers;

    public function __construct()
    {
        $this->productCategories = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Seller
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set creditCardNumber
     *
     * @param string $creditCardNumber
     *
     * @return Seller
     */
    public function setCreditCardNumber($creditCardNumber)
    {
        $this->creditCardNumber = $creditCardNumber;

        return $this;
    }

    /**
     * Get creditCardNumber
     *
     * @return string
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Seller
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set typeOfVehicle
     *
     * @param string $typeOfVehicle
     *
     * @return Seller
     */
    public function setTypeOfVehicle($typeOfVehicle)
    {
        $this->typeOfVehicle = $typeOfVehicle;

        return $this;
    }

    /**
     * Get typeOfVehicle
     *
     * @return string
     */
    public function getTypeOfVehicle()
    {
        return $this->typeOfVehicle;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Seller
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set productCategories
     *
     * @param \AppBundle\Entity\ProductCategory $productCategories
     *
     * @return Seller
     */
    public function setProductCategories(\AppBundle\Entity\ProductCategory $productCategories = null)
    {
        $this->productCategories = $productCategories;

        return $this;
    }

    /**
     * Get productCategories
     *
     * @return \AppBundle\Entity\ProductCategory
     */
    public function getProductCategories()
    {
        return $this->productCategories;
    }

    /**
     * Add productCategory
     *
     * @param \AppBundle\Entity\ProductCategory $productCategory
     *
     * @return Seller
     */
    public function addProductCategory(\AppBundle\Entity\ProductCategory $productCategory)
    {
        $this->productCategories[] = $productCategory;

        return $this;
    }

    /**
     * Remove productCategory
     *
     * @param \AppBundle\Entity\ProductCategory $productCategory
     */
    public function removeProductCategory(\AppBundle\Entity\ProductCategory $productCategory)
    {
        $this->productCategories->removeElement($productCategory);
    }

    /**
     * Add offer
     *
     * @param \AppBundle\Entity\Offer $offer
     *
     * @return Seller
     */
    public function addOffer(\AppBundle\Entity\Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \AppBundle\Entity\Offer $offer
     */
    public function removeOffer(\AppBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }
}
