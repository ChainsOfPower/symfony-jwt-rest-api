<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.11.2016.
 * Time: 3:11
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lat')
                ->add('lon')
                ->add('profilePictureFile', VichImageType::class)
                ->remove('email')
                ->remove('username')
                ->remove('current_password');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}