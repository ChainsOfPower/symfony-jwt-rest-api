<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 11.11.2016.
 * Time: 8:06
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lat')
                ->add('lon')
                ->add('role', ChoiceType::class, array(
                    'choices' => array(
                        'seller'    => 'seller',
                        'deliverer' => 'deliverer',
                        'customer'  => 'customer'
                    ),
                    'required'  => true,
                    'mapped'    => false,
                    'invalid_message'   => 'role is incorrect',
                ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}